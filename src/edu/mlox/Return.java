package edu.mlox;

public class Return extends RuntimeException {
    final LoxObject value;

    public Return(LoxObject value) {
        super(null, null, false, false);
        this.value = value;
    }
}