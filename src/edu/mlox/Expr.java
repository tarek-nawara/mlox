package edu.mlox;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Expr {
    abstract <R> R accept(Visitor<R> visitor);

    interface Visitor<R> {
        R visitGrouping(Grouping expr);

        R visitUnary(Unary expr);

        R visitLogical(Logical expr);

        R visitBinaryExpr(Binary expr);


        R visitVariableExpr(Variable expr);

        R visitAssignExpr(Assign expr);

        R visitGetIndexExpr(GetIndex expr);

        R visitCallExpr(Call expr);

        R visitGetPropertyExpr(GetProperty expr);

        R visitSetPropertyExpr(SetProperty expr);

        R visitSetIndexExpr(SetIndex expr);

        R visitNumberExpr(MLoxNumber expr);

        R visitMLoxStringExpr(MLoxString expr);

        R visitMLoxBooleanExpr(MLoxBoolean expr);

        R visitFnExpr(Fn expr);

        R visitMLoxNilExpr(MLoxNil expr);

        R visitThisExpr(This expr);

        R visitSuperExpr(Super expr);

        R visitMLoxListExpr(MLoxList expr);

        R visitMLoxMapExpr(MLoxMap expr);
    }

    static class Grouping extends Expr {
        final Expr expr;

        Grouping(Expr expr) {
            this.expr = expr;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitGrouping(this);
        }
    }

    static class Unary extends Expr {
        final Token operator;
        final Expr right;

        Unary(Token operator, Expr right) {
            this.operator = operator;
            this.right = right;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitUnary(this);
        }
    }

    static class MLoxNil extends Expr {
        MLoxNil() {
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitMLoxNilExpr(this);
        }
    }

    static class This extends Expr {
        final Token keyword;
        This(Token keyword) {
            this.keyword = keyword;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitThisExpr(this);
        }
    }

    static class Super extends Expr {
        final Token keyword;
        final Token method;

        Super(Token keyword, Token method) {
            this.keyword = keyword;
            this.method = method;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitSuperExpr(this);
        }
    }



    static class MLoxNumber extends Expr {
        final Object value;

        MLoxNumber(Object value) {
            this.value = value;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitNumberExpr(this);
        }
    }

    static class MLoxString extends Expr {
        final Object value;

        MLoxString(Object value) {
            this.value = value;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitMLoxStringExpr(this);
        }
    }

    static class MLoxBoolean extends Expr {
        final Object value;

        MLoxBoolean(Object value) {
            this.value = value;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitMLoxBooleanExpr(this);
        }
    }

    static class Fn extends Expr {
        final Token paren;
        final List<Token> params;
        final List<Stmt> body;

        Fn(final Token paren, List<Token> params, List<Stmt> body) {
            this.params = params;
            this.paren = paren;
            this.body = body;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitFnExpr(this);
        }
    }



    static class Logical extends Expr {
        final Expr left;
        final Token operator;
        final Expr right;

        Logical(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitLogical(this);
        }
    }

    static class Binary extends Expr {
        final Expr left;
        final Token operator;
        final Expr right;

        Binary(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitBinaryExpr(this);
        }
    }

    static class Variable extends Expr {
        final Token name;

        Variable(Token name) {
            this.name = name;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitVariableExpr(this);
        }
    }

    static class Assign extends Expr {
        final Token name;
        final Expr value;

        Assign(Token name, Expr value) {
            this.name = name;
            this.value = value;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitAssignExpr(this);
        }
    }

    public static class GetIndex extends Expr {
        final Expr object;
        final Token bracket;
        final List<Expr> index;

        public GetIndex(Expr object, Token bracket, List<Expr> index) {
            this.object = object;
            this.bracket = bracket;
            this.index = index;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitGetIndexExpr(this);
        }
    }

    public static class Call extends Expr {
        final Expr callee;
        final Token paren;
        final List<Expr> arguments;

        public Call(Expr callee, Token paren, List<Expr> arguments) {
            this.callee = callee;
            this.paren = paren;
            this.arguments = arguments;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitCallExpr(this);
        }
    }

    public static class GetProperty extends Expr {
        final Expr object;
        final Token name;

        public GetProperty(Expr object, Token name) {
            this.object = object;
            this.name = name;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitGetPropertyExpr(this);
        }
    }

    public static class SetProperty extends Expr {
        final Expr object;
        final Token name;
        final Expr value;

        public SetProperty(Expr object, Token name, Expr value) {
            this.object = object;
            this.name = name;
            this.value = value;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitSetPropertyExpr(this);
        }
    }

    public static class SetIndex extends Expr {
        final Expr object;
        final Token bracket;
        final List<Expr> index;
        final Expr value;

        public SetIndex(Expr object, Token bracket, List<Expr> index, Expr value) {
            this.object = object;
            this.bracket = bracket;
            this.index = index;
            this.value = value;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitSetIndexExpr(this);
        }
    }

    public static class MLoxList extends Expr {
        final Token bracket;
        List<Expr> elements;
        public MLoxList(Token bracket, List<Expr> elems) {
            this.bracket = bracket;
            this.elements = elems;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitMLoxListExpr(this);
        }
    }

    public static class MLoxMap extends Expr {
        final Token bracket;
        final Map<Expr, Expr> elems;
        public MLoxMap(Token bracket, Map<Expr, Expr> elems) {
            this.bracket = bracket;
            this.elems = elems;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitMLoxMapExpr(this);
        }
    }
}
