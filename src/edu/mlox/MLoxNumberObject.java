package edu.mlox;

import java.util.Objects;

public class MLoxNumberObject implements LoxObject {
    public double value;
    public MLoxNumberObject(double value) {
        this.value = value;
    }

    @Override
    public LoxObject get(Token name) {
        throw new RuntimeError(name, String.format("Undefined property '%s'.", name.lexeme()));
    }

    @Override
    public void set(Token name, LoxObject value) {
        throw new RuntimeError(name, String.format("Can't set property for lox number, property '%s'.", name.lexeme()));
    }

    @Override
    public boolean has(Token name) {
        return false;
    }

    public LoxObject neg() {
        return new MLoxNumberObject(-value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MLoxNumberObject that = (MLoxNumberObject) o;
        return Double.compare(that.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
