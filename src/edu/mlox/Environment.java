package edu.mlox;

import java.util.HashMap;
import java.util.Map;

public class Environment {
    final Environment enclosing;
    private final Map<String, LoxObject> values = new HashMap<>();

    public Environment(Environment enclosing) {
        this.enclosing = enclosing;
    }

    public Environment() {
        this(null);
    }

    void define(String name, LoxObject value) {
        values.put(name, value);
    }

    public LoxObject getAt(int distance, String name) {
        return ancestor(distance).values.get(name);
    }

    public LoxObject get(Token name) {
        if (values.containsKey(name.lexeme())) {
            return values.get(name.lexeme());
        }

        if (enclosing != null) return enclosing.get(name);

        throw new RuntimeError(name, String.format("Undefined variable '%s'.", name.lexeme()));
    }

    public void assign(Token name, LoxObject value) {
        if (values.containsKey(name.lexeme())) {
            values.put(name.lexeme(), value);
            return;
        }

        if (enclosing != null) {
            enclosing.assign(name, value);
            return;
        }

        throw new RuntimeError(name, String.format("Undefined variable '%s'.", name.lexeme()));
    }

    public void assignAt(int distance, Token name, LoxObject value) {
        ancestor(distance).values.put(name.lexeme(), value);
    }

    Environment ancestor(int distance) {
        var environment = this;
        for (int i = 0; i < distance; ++i) {
            environment = environment.enclosing;
        }

        return environment;
    }
}
