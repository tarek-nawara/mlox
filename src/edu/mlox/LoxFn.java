package edu.mlox;

import java.util.List;

public class LoxFn implements LoxCallable {
    private final Expr.Fn declaration;
    private final Environment closure;

    public LoxFn(Expr.Fn declaration, Environment closure) {
        this.declaration = declaration;
        this.closure = closure;
    }


    @Override
    public int arity() {
        return declaration.params.size();
    }

    @Override
    public LoxObject call(Interpreter interpreter, List<LoxObject> arguments, Token paren) {
        var environment = new Environment(closure);
        for (int i = 0; i < declaration.params.size(); ++i) {
            environment.define(declaration.params.get(i).lexeme(), arguments.get(i));
        }

        try {
            interpreter.executeBlock(declaration.body, environment);
        } catch (Return returnValue) {
            return returnValue.value;
        }
        return null;
    }

    @Override
    public String toString() {
        return "<fn anonymous>";
    }

    @Override
    public LoxObject get(Token name) {
        throw new RuntimeError(name, String.format("Undefined property '%s'.", name.lexeme()));
    }

    @Override
    public void set(Token name, LoxObject value) {
        throw new RuntimeError(name, String.format("Can't set property for anonymous function, property '%s'.", name.lexeme()));
    }

    @Override
    public boolean has(Token name) {
        return false;
    }
}