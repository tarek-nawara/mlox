package edu.mlox;

import java.util.HashMap;
import java.util.Map;

public class LoxRuntimeObject implements LoxObject {
    private LoxClass klass;
    private final Map<String, LoxObject> fields = new HashMap<>();

    public LoxRuntimeObject(LoxClass klass) {
        this.klass = klass;
    }

    @Override
    public LoxObject get(Token name) {
        if (fields.containsKey(name.lexeme())) {
            return fields.get(name.lexeme());
        }

        var method = klass.findMethod(name.lexeme());
        if (method != null) return method.bind(this);

        throw new RuntimeError(name, String.format("Undefined property '%s'.", name.lexeme()));
    }

    @Override
    public void set(Token name, LoxObject value) {
        fields.put(name.lexeme(), value);
    }

    @Override
    public boolean has(Token name) {
        return fields.containsKey(name.lexeme()) || (klass.findMethod(name.lexeme()) != null);
    }

    @Override
    public String toString() {
        return klass.name + " instance";
    }
}
