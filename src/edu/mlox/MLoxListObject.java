package edu.mlox;

import java.util.List;

public class MLoxListObject implements LoxObject {
    List<LoxObject> values;
    public MLoxListObject(List<LoxObject> values) {
        this.values = values;
    }

    @Override
    public LoxObject get(Token name) {
        throw new RuntimeError(name, String.format("Undefined property '%s'.", name.lexeme()));
    }

    @Override
    public void set(Token name, LoxObject value) {
        throw new RuntimeError(name, String.format("Can't set property for lox list, property '%s'.", name.lexeme()));
    }

    @Override
    public boolean has(Token name) {
        return false;
    }
}
