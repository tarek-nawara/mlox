package edu.mlox;

import java.util.Objects;

public class MLoxBooleanObject implements LoxObject {
    boolean value;
    public MLoxBooleanObject(boolean value) {
        this.value = value;
    }

    @Override
    public LoxObject get(Token name) {
        return null;
    }

    @Override
    public void set(Token name, LoxObject value) {
        throw new RuntimeError(name, String.format("Can't set property for lox boolean, property '%s'.", name.lexeme()));
    }

    @Override
    public boolean has(Token name) {
        return false;
    }

    public LoxObject not() {
        return new MLoxBooleanObject(!value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MLoxBooleanObject that = (MLoxBooleanObject) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
