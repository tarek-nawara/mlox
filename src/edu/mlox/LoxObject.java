package edu.mlox;

public interface LoxObject {
    LoxObject get(Token name);

    void set(Token name, LoxObject value);

    boolean has(Token name);
}
