package edu.mlox;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class Resolver implements Expr.Visitor<Void>, Stmt.Visitor<Void> {
    private final Interpreter interpreter;
    private final Stack<Map<String, Boolean>> scopes = new Stack<>();

    private FunctionType currentFunction = FunctionType.NONE;
    private ClassType currentClass = ClassType.NONE;

    public Resolver(Interpreter interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public Void visitGrouping(Expr.Grouping expr) {
        resolve(expr.expr);
        return null;
    }

    @Override
    public Void visitUnary(Expr.Unary expr) {
        resolve(expr.right);
        return null;
    }

    @Override
    public Void visitLogical(Expr.Logical expr) {
        resolve(expr.left);
        resolve(expr.right);
        return null;
    }

    @Override
    public Void visitBinaryExpr(Expr.Binary expr) {
        resolve(expr.left);
        resolve(expr.right);
        return null;
    }

    @Override
    public Void visitVariableExpr(Expr.Variable expr) {
        if (!scopes.isEmpty() && scopes.peek().get(expr.name.lexeme()) == Boolean.FALSE) {
            MLox.error(expr.name, "Can't read local variable in its own initializer");
        }
        resolveLocal(expr, expr.name);
        return null;
    }

    @Override
    public Void visitAssignExpr(Expr.Assign expr) {
        resolve(expr.value);
        resolveLocal(expr, expr.name);
        return null;
    }

    @Override
    public Void visitGetIndexExpr(Expr.GetIndex expr) {
        resolve(expr.object);
        for (var i : expr.index) {
            resolve(i);
        }
        return null;
    }

    @Override
    public Void visitCallExpr(Expr.Call expr) {
        resolve(expr.callee);
        for (var argument : expr.arguments) {
            resolve(argument);
        }
        return null;
    }

    @Override
    public Void visitGetPropertyExpr(Expr.GetProperty expr) {
        resolve(expr.object);
        return null;
    }

    @Override
    public Void visitSetPropertyExpr(Expr.SetProperty expr) {
        resolve(expr.object);
        resolve(expr.value);
        return null;
    }

    @Override
    public Void visitSetIndexExpr(Expr.SetIndex expr) {
        resolve(expr.object);
        resolve(expr.value);
        for (var i : expr.index) {
            resolve(i);
        }
        return null;
    }

    @Override
    public Void visitNumberExpr(Expr.MLoxNumber expr) {
        return null;
    }

    @Override
    public Void visitMLoxStringExpr(Expr.MLoxString expr) {
        return null;
    }

    @Override
    public Void visitMLoxBooleanExpr(Expr.MLoxBoolean expr) {
        return null;
    }

    @Override
    public Void visitFnExpr(Expr.Fn expr) {
        resolveFn(expr);
        return null;
    }

    @Override
    public Void visitMLoxNilExpr(Expr.MLoxNil expr) {
        return null;
    }

    @Override
    public Void visitThisExpr(Expr.This expr) {
        if (currentClass == ClassType.NONE) {
            MLox.error(expr.keyword, "Can't use 'this' outside of a class.");
            return null;
        }

        resolveLocal(expr, expr.keyword);
        return null;
    }

    @Override
    public Void visitSuperExpr(Expr.Super expr) {
        return null;
    }

    @Override
    public Void visitMLoxListExpr(Expr.MLoxList expr) {
        for (var elem : expr.elements) {
            resolve(elem);
        }
        return null;
    }

    @Override
    public Void visitMLoxMapExpr(Expr.MLoxMap expr) {
        for (var entry : expr.elems.entrySet()) {
            resolve(entry.getKey());
            resolve(entry.getValue());
        }
        return null;
    }

    @Override
    public Void visitFunctionStmt(Stmt.Function stmt) {
        declare(stmt.name);
        define(stmt.name);
        resolveFunction(stmt, FunctionType.Function);
        return null;
    }

    @Override
    public Void visitPrintStmt(Stmt.Print stmt) {
        return null;
    }

    @Override
    public Void visitReturnStmt(Stmt.Return stmt) {
        return null;
    }

    @Override
    public Void visitExpressionStmt(Stmt.Expression stmt) {
        return null;
    }

    @Override
    public Void visitVarStmt(Stmt.Var stmt) {
        return null;
    }

    @Override
    public Void visitBlockStmt(Stmt.Block stmt) {
        return null;
    }

    @Override
    public Void visitClassStmt(Stmt.Class stmt) {
        return null;
    }

    @Override
    public Void visitWhileStmt(Stmt.While stmt) {
        return null;
    }

    @Override
    public Void visitIfStmt(Stmt.If stmt) {
        return null;
    }


    // ------------ Helper methods ----------------- //
    private void resolve(Expr expr) {
        expr.accept(this);
    }

    private void resolve(Stmt stmt) {
        stmt.accept(this);
    }

    private void resolve(List<Stmt> statements) {
        for (var stmt : statements) {
            resolve(stmt);
        }
    }

    private void resolveLocal(Expr expr, Token name) {
        for (int i = scopes.size() - 1; i >= 0; --i) {
            var scope = scopes.get(i);
            if (scope.containsKey(name.lexeme())) {
                interpreter.resolve(expr, scopes.size() - i - 1);
                return;
            }
        }
    }

    private void resolveFunction(Stmt.Function function, FunctionType type) {
        var enclosing = currentFunction;
        currentFunction = type;

        beginScope();
        for (var param : function.params) {
            declare(param);
            define(param);
        }

        resolve(function.body);
        endScope();
        currentFunction = enclosing;
    }

    private void resolveFn(Expr.Fn fn) {
        var enclosing = currentFunction;
        currentFunction = FunctionType.FN;

        beginScope();
        for (var param : fn.params) {
            declare(param);
            define(param);
        }

        resolve(fn.body);
        endScope();
        currentFunction = enclosing;
    }

    private void declare(Token name) {
        if (scopes.isEmpty()) return;
        var currentScope = scopes.peek();
        if (currentScope.containsKey(name.lexeme())) {
            MLox.error(name, "Already a variable with this name in this scope");
        }

        currentScope.put(name.lexeme(), false);
    }

    private void define(Token name) {
        if (scopes.isEmpty()) return;
        scopes.peek().put(name.lexeme(), true);
    }

    private void beginScope() {
        scopes.push(new HashMap<>());
    }

    private void endScope() {
        scopes.pop();
    }

}
