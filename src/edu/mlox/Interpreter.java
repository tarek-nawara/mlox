package edu.mlox;

import java.util.*;

public class Interpreter implements Expr.Visitor<LoxObject>, Stmt.Visitor<Void> {
    private final Environment globals = new Environment();
    private final Map<Expr, Integer> locals = new HashMap<>();
    private Environment environment = globals;

    /**
     * Interpreter entry point.
     *
     * @param statements list of statements to execute.
     */
    public void interpret(List<Stmt> statements) {
        try {
            for (var statement : statements) {
                execute(statement);
            }
        } catch (RuntimeError error) {
            MLox.runtimeError(error);
        }
    }

    private void execute(Stmt statement) {
        statement.accept(this);
    }

    @Override
    public Void visitFunctionStmt(Stmt.Function stmt) {
        return null;
    }

    @Override
    public Void visitPrintStmt(Stmt.Print stmt) {
        return null;
    }

    @Override
    public Void visitReturnStmt(Stmt.Return stmt) {
        return null;
    }

    @Override
    public Void visitExpressionStmt(Stmt.Expression stmt) {
        return null;
    }

    @Override
    public Void visitVarStmt(Stmt.Var stmt) {
        return null;
    }

    @Override
    public Void visitBlockStmt(Stmt.Block stmt) {
        return null;
    }

    @Override
    public Void visitClassStmt(Stmt.Class stmt) {
        return null;
    }

    @Override
    public Void visitWhileStmt(Stmt.While stmt) {
        return null;
    }

    @Override
    public Void visitIfStmt(Stmt.If stmt) {
        return null;
    }


    // ------------- Expression evaluation ----- //
    private LoxObject evaluate(Expr expression) {
        return expression.accept(this);
    }

    @Override
    public LoxObject visitGrouping(Expr.Grouping expr) {
        return evaluate(expr.expr);
    }

    @Override
    public LoxObject visitUnary(Expr.Unary expr) {
        var right = evaluate(expr.right);
        switch (expr.operator.type()) {
            case BANG:
                return isTruthy(right).not();
            case MINUS:
                if (!(right instanceof MLoxNumberObject numberObject)) {
                    throw new RuntimeError(expr.operator, "Operand must be a number.");
                }
                return numberObject.neg();
        }

        // Unreachable
        return null;
    }

    @Override
    public LoxObject visitLogical(Expr.Logical expr) {
        var left = evaluate(expr.left);
        if (expr.operator.type() == TokenType.OR) {
            if (isTruthy(left).value) return left;
        } else {
            if (!isTruthy(left).value) return left;
        }

        return evaluate(expr.right);
    }

    @Override
    public LoxObject visitBinaryExpr(Expr.Binary expr) {
        var left = evaluate(expr.left);
        var right = evaluate(expr.right);

        return switch (expr.operator.type()) {
            case GREATER       -> new MLoxBooleanObject(evaluateCmp(left, right, expr.operator).value > 0);
            case GREATER_EQUAL -> new MLoxBooleanObject(evaluateCmp(left, right, expr.operator).value >= 0);
            case LESS          -> new MLoxBooleanObject(evaluateCmp(left, right, expr.operator).value < 0);
            case LESS_EQUAL    -> new MLoxBooleanObject(evaluateCmp(left, right, expr.operator).value <= 0);
            case BANG_EQUAL    -> evaluateEqual(left, right, expr.operator).not();
            case EQUAL_EQUAL   -> evaluateEqual(left, right, expr.operator);
            case MINUS         -> evaluateMethod("__subtract", left, right, expr.operator);
            case PLUS          -> evaluateMethod("__add", left, right, expr.operator);
            case SLASH         -> evaluateMethod("__div", left, right, expr.operator);
            case STAR          -> evaluateMethod("__multiply", left, right, expr.operator);
            default            -> null;
        };

    }

    @Override
    public LoxObject visitVariableExpr(Expr.Variable expr) {
        return lookUpVariable(expr.name, expr);
    }

    @Override
    public LoxObject visitAssignExpr(Expr.Assign expr) {
        var value = evaluate(expr.value);
        var distance = locals.get(expr);
        if (distance != null) {
            environment.assignAt(distance, expr.name, value);
        } else {
            globals.assign(expr.name, value);
        }
        return value;
    }

    @Override
    public LoxObject visitGetIndexExpr(Expr.GetIndex expr) {
        var instance = evaluate(expr.object);
        var methodName = Token.fromLexeme("__get_index");
        if (!instance.has(methodName)) {
            throw new RuntimeError(expr.bracket, "no suitable __get_index method available on the given instance.");
        }

        var method = instance.get(methodName);
        if (!(method instanceof LoxFunction function)) {
            throw new RuntimeError(expr.bracket, "__get_index must be a function.");
        }

        if (function.arity() != expr.index.size()) {
            throw new RuntimeError(expr.bracket, String.format("__get_index doesn't have required number of parameters %d.", expr.index.size()));
        }

        var indexValues = new ArrayList<LoxObject>();
        for (var i : expr.index) {
            indexValues.add(evaluate(i));
        }

        return function.call(this, indexValues, expr.bracket);
    }

    @Override
    public LoxObject visitCallExpr(Expr.Call expr) {
        var callee = evaluate(expr.callee);
        if (!(callee instanceof LoxCallable function)) {
            throw new RuntimeError(expr.paren, "Can only call functions and classes.");
        }

        if (expr.arguments.size() != function.arity()) {
            throw new RuntimeError(expr.paren,
                    String.format("Expected %d arguments but got %d.", function.arity(), expr.arguments.size()));
        }

        var arguments = new ArrayList<LoxObject>();
        for (var argument : expr.arguments) {
            arguments.add(evaluate(argument));
        }

        return function.call(this, arguments, expr.paren);
    }

    @Override
    public LoxObject visitGetPropertyExpr(Expr.GetProperty expr) {
        var instance = evaluate(expr.object);
        return instance.get(expr.name);
    }

    @Override
    public LoxObject visitSetPropertyExpr(Expr.SetProperty expr) {
        var object = evaluate(expr.object);
        var value = evaluate(expr.value);
        object.set(expr.name, value);
        return value;
    }

    @Override
    public LoxObject visitSetIndexExpr(Expr.SetIndex expr) {
        var instance = evaluate(expr.object);
        var methodName = Token.fromLexeme("__set_index");
        if (!instance.has(methodName)) {
            throw new RuntimeError(expr.bracket, "no suitable set_index method available on the given instance.");
        }

        var method = instance.get(methodName);
        if (!(method instanceof LoxFunction function)) {
            throw new RuntimeError(expr.bracket, "__set_index must be a function.");
        }

        if (function.arity() != expr.index.size() + 1) {
            throw new RuntimeError(expr.bracket, String.format("__set_index doesn't have required number of parameters %d.", expr.index.size() + 1));
        }

        var indexValues = new ArrayList<LoxObject>();
        for (var i : expr.index) {
            indexValues.add(evaluate(i));
        }

        var arguments = new ArrayList<>(indexValues);
        arguments.add(evaluate(expr.value));
        return function.call(this, arguments, expr.bracket);
    }

    @Override
    public LoxObject visitNumberExpr(Expr.MLoxNumber expr) {
        return new MLoxNumberObject((double) expr.value);
    }

    @Override
    public LoxObject visitMLoxStringExpr(Expr.MLoxString expr) {
        return new MLoxStringObject((String) expr.value);
    }

    @Override
    public LoxObject visitMLoxBooleanExpr(Expr.MLoxBoolean expr) {
        return new MLoxBooleanObject((boolean) expr.value);
    }

    @Override
    public LoxObject visitMLoxListExpr(Expr.MLoxList expr) {
        var values = new ArrayList<LoxObject>();
        for (var e : expr.elements) {
            values.add(evaluate(e));
        }
        return new MLoxListObject(values);
    }

    @Override
    public LoxObject visitMLoxMapExpr(Expr.MLoxMap expr) {
        var values = new HashMap<MLoxNumberObject, MLoxMapObject.Entry>();
        for (var e : expr.elems.entrySet()) {
            var key = evaluate(e.getKey());
            var value = evaluate(e.getValue());
            var name = Token.fromLexeme("__hash");
            if (key.has(name) && (key.get(name) instanceof LoxFunction hashFunction)) {
                if (hashFunction.arity() != 0) {
                    throw new RuntimeError(expr.bracket, "__hash function must have zero arguments.");
                }
                var hash = hashFunction.call(this, Collections.emptyList(), expr.bracket);
                if (!(hash instanceof MLoxNumberObject numberObject)) {
                    throw new RuntimeError(expr.bracket, "__hash function must return a valid hash value");
                }

                values.put(numberObject, new MLoxMapObject.Entry(key, value));
            } else {
                values.put(new MLoxNumberObject(key.hashCode()), new MLoxMapObject.Entry(key, value));
            }
        }

        return new MLoxMapObject(values);
    }

    @Override
    public LoxObject visitFnExpr(Expr.Fn expr) {
        return new LoxFn(expr, environment);
    }

    @Override
    public LoxObject visitMLoxNilExpr(Expr.MLoxNil expr) {
        return MLoxNilObject.NIL;
    }

    @Override
    public LoxObject visitThisExpr(Expr.This expr) {
        return lookUpVariable(expr.keyword, expr);
    }

    @Override
    public LoxObject visitSuperExpr(Expr.Super expr) {
        return null;
    }


    // ------------ Helper methods ----- //
    private MLoxNumberObject evaluateCmp(LoxObject left, LoxObject right, Token operator) {
        var result = evaluateMethod("__cmp", left, right, operator);
        if (!(result instanceof MLoxNumberObject numberObject)) {
            throw new RuntimeError(operator, "__cmp method must have numeric result.");
        }

        return numberObject;
    }

    private MLoxBooleanObject evaluateEqual(LoxObject left, LoxObject right, Token operator) {
        var result = evaluateMethod("__equal", left, right, operator);
        if (!(result instanceof MLoxBooleanObject booleanObject)) {
            throw new RuntimeError(operator, "__equal method must have numeric result.");
        }

        return booleanObject;
    }

    private LoxObject evaluateMethod(String name, LoxObject left, LoxObject right, Token operator) {
        Token token = Token.fromLexeme(name);
        if (!left.has(token) || !(left.get(token) instanceof LoxFunction function)) {
            throw new RuntimeError(operator, String.format("No suitable %s method available on target object.", name));
        }

        if (function.arity() != 1) {
            throw new RuntimeError(operator, String.format("method %s must have exactly 1 argument", name));
        }

        return function.call(this, List.of(right), operator);
    }

    private LoxObject lookUpVariable(Token name, Expr expr) {
        var distance = locals.get(expr);
        if (distance != null) {
            return environment.getAt(distance, name.lexeme());
        } else {
            return globals.get(name);
        }
    }

    private MLoxBooleanObject isTruthy(Object object) {
        if (object == null) return new MLoxBooleanObject(false);
        if (object instanceof MLoxBooleanObject instance) return instance;
        return new MLoxBooleanObject(true);
    }

    public void executeBlock(List<Stmt> statements, Environment environment) {
        var previous = this.environment;
        try {
            this.environment = environment;
            for (var statement : statements) {
                execute(statement);
            }
        } finally {
            this.environment = previous;
        }
    }

    public void resolve(Expr expr, int depth) {
        locals.put(expr, depth);
    }
}
