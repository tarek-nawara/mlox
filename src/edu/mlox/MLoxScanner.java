package edu.mlox;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.mlox.TokenType.*;

public class MLoxScanner {
    private static final Map<String, TokenType> keywords;

    private final String source;
    private final List<Token> tokens;

    private int start;
    private int current;
    private int line = 1;

    static {
        keywords = new HashMap<>();
        keywords.put("and", AND);
        keywords.put("class", CLASS);
        keywords.put("else", ELSE);
        keywords.put("false", FALSE);
        keywords.put("for", FOR);
        keywords.put("fun", FUN);
        keywords.put("fn", FN);
        keywords.put("if", IF);
        keywords.put("nil", NIL);
        keywords.put("or", OR);
        keywords.put("print", PRINT);
        keywords.put("return", RETURN);
        keywords.put("range", RANGE);
        keywords.put("super", SUPER);
        keywords.put("this", THIS);
        keywords.put("true", TRUE);
        keywords.put("let", LET);
        keywords.put("while", WHILE);
    }

    public MLoxScanner(String source) {
        this.source = source;
        this.tokens = new ArrayList<>();
    }

    public List<Token> scanTokens() {
        while (!isAtEnd()) {
            start = current;
            scanToken();
        }

        tokens.add(new Token(EOF, "", null, line));
        return tokens;
    }

    private void scanToken() {
        char c = advance();
        switch (c) {
            case '(': addToken(LEFT_PAREN); break;
            case ')': addToken(RIGHT_PAREN); break;
            case '{': addToken(LEFT_BRACE); break;
            case '}': addToken(RIGHT_BRACE); break;
            case ',': addToken(COMMA); break;
            case '.': addToken(DOT); break;
            case '-': addToken(MINUS); break;
            case '+': addToken(PLUS); break;
            case ';': addToken(SEMICOLON); break;
            case '*': addToken(STAR); break;
            case '[': addToken(LEFT_BRACKET); break;
            case ']': addToken(RIGHT_BRACKET); break;
            case ':': addToken(COLON); break;
            case '#':
                if (match('{')) {
                    addToken(MAP_OPEN);
                } else {
                    load();
                }
                break;
            case '!':
                addToken(match('=') ? BANG_EQUAL: BANG);
                break;
            case '=':
                addToken(match('=') ? EQUAL_EQUAL : EQUAL);
                break;
            case '<':
                addToken(match('=') ? LESS_EQUAL : LESS);
                break;
            case '>':
                addToken(match('=') ? GREATER_EQUAL : GREATER);
                break;
            case '/':
                if (match('/')) {
                    // a comment goes until the end of the line.
                    while (peek() != '\n' && !isAtEnd()) advance();
                } else {
                    addToken(SLASH);
                }
                break;
            case ' ':
            case '\r':
            case '\t':
                // Ignore whitespace.
                break;
            case '\n':
                ++line;
                break;
            case '"': string(); break;
            case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9': number(); break;
            default:
                if (isAlpha(c)) {
                    identifier();
                } else {
                    MLox.error(line, "unexpected character.");
                }
                break;
        }
    }

    private void identifier() {
        while (isAlphaNumeric(peek())) advance();
        var text = source.substring(start, current);
        var type = keywords.getOrDefault(text, IDENTIFIER);
        addToken(type);
    }

    private void number() {
        while (Character.isDigit(peek())) advance();

        // Look for a fractional part.
        if (peek() == '.' && Character.isDigit(peekNext())) {
            advance();
            while (Character.isDigit(peek())) advance();
        }
        var value = Double.parseDouble(source.substring(start, current));
        addToken(NUMBER, value);
    }

    private void string() {
        while (peek() != '"' && !isAtEnd()) {
            if (peek() == '\n') ++line;
            advance();
        }
        if (isAtEnd()) {
            MLox.error(line, "Unterminated string.");
            return;
        }
        advance(); // The closing "
        var value = source.substring(start + 1, current - 1);
        addToken(STRING, value);
    }

    private void load() {
        while (isAlphaNumeric(peek())) advance();
        var lexeme = source.substring(start, current);
        if ("load".equals(lexeme)) {
            MLox.error(line, "Invalid identifier.");
        }

        var pathStart = current;
        while (!isAtEnd() && peek() != ';') {
            advance();
        }

        advance();  // Ending semicolon

        var path = source.substring(pathStart, current).trim().replaceAll("\\.", File.pathSeparator) + ".lox";
        try {
            var bytes = Files.readAllBytes(Paths.get(path));
            var nextScanner = new MLoxScanner(new String(bytes, Charset.defaultCharset()));
            tokens.addAll(nextScanner.scanTokens());
        } catch (IOException e) {
            MLox.error(line, "Load path not found.");
        }
    }

    private void addToken(TokenType type) {
        addToken(type, null);
    }

    private void addToken(TokenType type, Object literal) {
        var lexeme = source.substring(start, current);
        tokens.add(new Token(type, lexeme, literal, line));
    }

    // ------------ Helper functions ----- //
    private boolean match(char expected) {
        if (peek() != expected) return false;
        advance();
        return true;
    }

    private char peek() {
        if (isAtEnd()) return '\0';
        return source.charAt(current);
    }

    private char peekNext() {
        if (current + 1 >= source.length()) return '\0';
        return source.charAt(current + 1);
    }

    private boolean isAtEnd() {
        return current >= source.length();
    }

    private char advance() {
        return source.charAt(current++);
    }

    private boolean isAlphaNumeric(char c) {
        return isAlpha(c) || isDigit(c);
    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isAlpha(char c) {
        return (c >= 'a' && c <= 'z') ||
                (c >= 'A' && c <= 'Z') ||
                c == '_';
    }
}
