package edu.mlox;

import java.util.List;

public interface LoxCallable extends LoxObject {
    int arity();

    LoxObject call(Interpreter interpreter, List<LoxObject> arguments, Token paren);
}
