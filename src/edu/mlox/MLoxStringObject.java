package edu.mlox;

import java.util.Objects;

public class MLoxStringObject implements LoxObject {
    String value;
    public MLoxStringObject(String value) {
        this.value = value;
    }

    @Override
    public LoxObject get(Token name) {
        return null;
    }

    @Override
    public void set(Token name, LoxObject value) {

    }

    @Override
    public boolean has(Token name) {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MLoxStringObject that = (MLoxStringObject) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
