package edu.mlox;

public class MLoxNilObject implements LoxObject {
    public static final MLoxNilObject NIL = new MLoxNilObject();

    @Override
    public LoxObject get(Token name) {
        throw new RuntimeError(name, "NilPointer exception");
    }

    @Override
    public void set(Token name, LoxObject value) {
        throw new RuntimeError(name, "NilPointer exception");
    }

    @Override
    public boolean has(Token name) {
        throw new RuntimeError(name, "NilPointer exception");
    }
}
