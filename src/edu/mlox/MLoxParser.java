package edu.mlox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static edu.mlox.TokenType.*;

public class MLoxParser {
    private static class ParseError extends RuntimeException {
    }

    private final List<Token> tokens;
    private int current = 0;

    public MLoxParser(List<Token> tokens) {
        this.tokens = tokens;
    }

    public List<Stmt> parse() {
        var statements = new ArrayList<Stmt>();
        while (!isAtEnd()) {
            statements.add(declaration());
        }

        return statements;
    }

    private Stmt declaration() {
        try {
            if (match(CLASS)) return classDeclaration();
            if (match(FUN))   return functionDeclaration("function");
            if (match(LET))   return varDeclaration();

            return statement();
        } catch (ParseError error) {
            synchronize();
            return null;
        }
    }

    private Stmt classDeclaration() {
        var name = consume(IDENTIFIER, "Expect class name.");
        consume(LEFT_BRACE, "Expect '{' before class body.");
        var methods = new ArrayList<Stmt.Function>();

        while (!isAtEnd() && !check(RIGHT_BRACE)) {
            methods.add(functionDeclaration("method"));
        }

        consume(RIGHT_BRACE, "Expect '}' after class body.");
        return new Stmt.Class(name, methods);
    }

    private Stmt.Function functionDeclaration(String kind) {
        var name = consume(IDENTIFIER, String.format("Expect %s name.", kind));
        consume(LEFT_PAREN, String.format("Expect '(' after %s name.", kind));
        var params = new ArrayList<Token>();
        if (!check(RIGHT_PAREN)) {
            do {
                params.add(consume(IDENTIFIER, "Expect parameter name."));
            } while (match(COMMA));
        }

        consume(RIGHT_PAREN, "Expect ')' after parameters.");
        consume(LEFT_BRACE, String.format("Expect '{' before %s body.", kind));
        var body = block();
        return new Stmt.Function(name, params, body);
    }

    private Stmt varDeclaration() {
        var name = consume(IDENTIFIER, "Expect variable name.");
        Expr initializer = null;
        if (match(EQUAL)) {
            initializer = expression();
        }

        consume(SEMICOLON, "Expect ';' after variable declaration.");
        return new Stmt.Var(name, initializer);
    }

    private Stmt statement() {
        return expressionStatement();
    }

    private List<Stmt> block() {
        var statements = new ArrayList<Stmt>();

        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            statements.add(declaration());
        }
        consume(RIGHT_BRACE, "Expect '}' after block.");
        return statements;
    }

    private Stmt expressionStatement() {
        var value = expression();
        consume(SEMICOLON, "Expected ';' after expression.");
        return new Stmt.Expression(value);
    }

    private Expr expression() {
        return assignment();
    }

    private Expr assignment() {
        var expr = or();
        if (match(EQUAL)) {
            var equals = previous();
            var value = assignment();
            if (expr instanceof Expr.Variable variable) {
                var name = variable.name;
                return new Expr.Assign(name, value);
            } else if (expr instanceof Expr.GetProperty getProperty) {
                return new Expr.SetProperty(getProperty.object, getProperty.name, value);
            } else if (expr instanceof Expr.GetIndex getIndex) {
                return new Expr.SetIndex(getIndex.object, getIndex.bracket, getIndex.index, value);
            }

            throw error(equals, "Invalid assignment target.");
        }

        return expr;
    }

    private Expr or() {
        var expr = and();
        while (match(OR)) {
            var operator = previous();
            var right = and();
            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr and() {
        var expr = equality();
        while (match(AND)) {
            var operator = previous();
            var right = equality();
            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr equality() {
        var expr = comparison();
        while (match(BANG_EQUAL, EQUAL_EQUAL)) {
            var operator = previous();
            var right = comparison();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr comparison() {
        var expr = term();
        while (match(GREATER, GREATER_EQUAL, LESS, LESS_EQUAL)) {
            var operator = previous();
            var right = term();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr term() {
        var expr = factor();
        while (match(MINUS, PLUS)) {
            var operator = previous();
            var right = factor();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr factor() {
        var expr = unary();
        while (match(SLASH, STAR, MOD)) {
            var operator = previous();
            var right = unary();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr unary() {
        if (match(BANG, MINUS)) {
            var operator = previous();
            var right = unary();
            return new Expr.Unary(operator, right);
        }

        return call();
    }

    private Expr call() {
        var expr = primary();
        while (true) {
            if (match(LEFT_PAREN)) {
                expr = finishCall(expr);
            } else if (match(DOT)) {
                var name = consume(IDENTIFIER, "Expect property name after '.'.");
                expr = new Expr.GetProperty(expr, name);
            } else if (match(LEFT_BRACKET)) {
                expr = finishIndex(expr);
            } else {
                break;
            }
        }

        return expr;
    }

    private Expr primary() {
        if (match(FN))           return fn();
        if (match(NIL))          return new Expr.MLoxNil();
        if (match(THIS))         return new Expr.This(previous());
        if (match(FALSE))        return new Expr.MLoxBoolean(false);
        if (match(TRUE))         return new Expr.MLoxBoolean(true);
        if (match(NUMBER))       return new Expr.MLoxNumber(previous().literal());
        if (match(STRING))       return new Expr.MLoxString(previous().literal());
        if (match(MAP_OPEN))     return map();
        if (match(IDENTIFIER))   return new Expr.Variable(previous());
        if (match(LEFT_BRACKET)) return list();

        if (match(LEFT_PAREN)) {
            var expr = expression();
            consume(RIGHT_PAREN, "Expected ')' after expression.");
            return new Expr.Grouping(expr);
        }

        throw error(peek(), "Expect expression.");
    }

    private Expr fn() {
        var paren = consume(LEFT_PAREN, "Expect '(' after anonymous function.");
        var params = new ArrayList<Token>();
        if (!check(RIGHT_PAREN)) {
            do {
                params.add(consume(IDENTIFIER, "Expect parameter name."));
            } while (match(COMMA));
        }

        consume(RIGHT_PAREN, "Expect ')' after parameters.");
        consume(LEFT_BRACE, "Expect '{' before anonymous function. body.");
        var body = block();
        return new Expr.Fn(paren, params, body);
    }


    private Expr finishCall(Expr callee) {
        var arguments = new ArrayList<Expr>();
        if (!check(RIGHT_PAREN)) {
            do {
                arguments.add(expression());
            } while(match(COMMA));
        }

        var paren = consume(RIGHT_PAREN, "Expect ')' after arguments.");
        return new Expr.Call(callee, paren, arguments);
    }

    private Expr finishIndex(Expr container) {
        var arguments = new ArrayList<Expr>();
        if (!check(RIGHT_BRACKET)) {
            do {
                arguments.add(expression());
            } while(match(COMMA));
        }

        var paren = consume(RIGHT_BRACKET, "Expect ']' after arguments.");
        return new Expr.GetIndex(container, paren, arguments);
    }

    private Expr list() {
        var bracket = previous();
        var elems = new ArrayList<Expr>();
        if (!check(RIGHT_BRACKET)) {
            do {
                elems.add(expression());
            } while(match(COMMA));
        }
        consume(RIGHT_BRACKET, "Expect ']' after list parameters.");
        return new Expr.MLoxList(bracket, elems);
    }

    private Expr map() {
        var bracket = previous();
        var elems = new HashMap<Expr, Expr>();
        if (!check(RIGHT_BRACE)) {
            do {
                var key = expression();
                consume(COLON, "Expect ':' after map key");
                var value = expression();
                elems.put(key, value);
            } while (match(COMMA));
        }
        consume(RIGHT_BRACE, "Expect '}' after map parameters.");
        return new Expr.MLoxMap(bracket, elems);
    }

    // ----------- Helper functions ----- //
    private void synchronize() {
        advance();
        while (!isAtEnd()) {
            if (previous().type() == SEMICOLON) return;

            switch (peek().type()) {
                case CLASS, FOR, FUN, IF, PRINT, RETURN, LET, WHILE:
                    return;
            }

            advance();
        }
    }

    private boolean match(TokenType... types) {
        for (var type : types) {
            if (check(type)) {
                advance();
                return true;
            }
        }
        return false;
    }

    private Token consume(TokenType type, String message) {
        if (check(type)) return advance();
        throw error(peek(), message);
    }

    private ParseError error(Token token, String message) {
        MLox.error(token, message);
        return new ParseError();
    }

    private Token advance() {
        if (!isAtEnd()) ++current;
        return previous();
    }

    private boolean check(TokenType type) {
        if (isAtEnd()) return false;
        return peek().type() == type;
    }

    private boolean isAtEnd() {
        return peek().type() == EOF;
    }

    private Token peek() {
        return tokens.get(current);
    }

    private Token previous() {
        return tokens.get(current - 1);
    }
}
