package edu.mlox;

import java.util.Map;

public class MLoxMapObject implements LoxObject {
    Map<MLoxNumberObject, MLoxMapObject.Entry> values;

    static class Entry {
        LoxObject key;
        LoxObject value;

        public Entry(LoxObject key, LoxObject value) {
            this.key = key;
            this.value = value;
        }
    }

    public MLoxMapObject(Map<MLoxNumberObject, MLoxMapObject.Entry> values) {
        this.values = values;
    }

    @Override
    public LoxObject get(Token name) {
        throw new RuntimeError(name, String.format("Undefined property '%s'.", name.lexeme()));
    }

    @Override
    public void set(Token name, LoxObject value) {
        throw new RuntimeError(name, String.format("Can't set property for lox map, property '%s'.", name.lexeme()));
    }

    @Override
    public boolean has(Token name) {
        return false;
    }
}
