package edu.mlox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MLox {
    private static boolean hadError;
    private static boolean hadRuntimeError;

    public static void main(String[] args) throws IOException {
        if (args.length > 1) {
            System.out.println("Usage mlox [script]");
        } else if (args.length == 1) {
            runFile(args[0]);
        } else {
            runPrompt();
        }
    }

    private static void runFile(String path) throws IOException {
        var bytes = Files.readAllBytes(Paths.get(path));
        run(new String(bytes, Charset.defaultCharset()));
    }

    private static void runPrompt() throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));

        for (;;) {
            System.out.print("> ");
            var line = reader.readLine();
            if (line == null) break;
            run(line);
            hadError = false;
        }
    }

    private static void run(String source) {
        var scanner = new MLoxScanner(source);
        var tokens = scanner.scanTokens();
        System.out.println(tokens);
        var parser = new MLoxParser(tokens);
        var statements = parser.parse();

        if (hadError) return;

    }

    static void error(int line, String message) {
        report(line, "", message);
    }

    static void error(Token token, String message) {
        if (token.type() == TokenType.EOF) {
            report(token.line(), "at end", message);
        } else {
            report(token.line(), String.format("at '%s'", token.lexeme()), message);
        }
    }

    private static void report(int line, String where, String message) {
        System.err.printf("[Line %d] Error %s: %s\n", line, where, message);
        hadError = true;
    }

    public static void runtimeError(RuntimeError error) {
        System.err.printf("%s\n[line %d]", error.getMessage(), error.token.line());
        hadRuntimeError = true;
    }
}
