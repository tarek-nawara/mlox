package edu.mlox;

import java.util.HashMap;
import java.util.List;

public class LoxClass implements LoxCallable {
    final String name;
    private final HashMap<String, LoxFunction> methods;

    public LoxClass(String name, HashMap<String, LoxFunction> methods) {
        this.name = name;
        this.methods = methods;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int arity() {
        var initializer = findMethod("init");
        if (initializer == null) return 0;
        return initializer.arity();
    }

    @Override
    public LoxObject call(Interpreter interpreter, List<LoxObject> arguments, Token paren) {
        var instance = new LoxRuntimeObject(this);
        var initializer = findMethod("init");
        if (initializer != null) {
            initializer.bind(instance).call(interpreter, arguments, paren);
        }

        return instance;
    }

    public LoxFunction findMethod(String name) {
        if (methods.containsKey(name)) {
            return methods.get(name);
        }

        return null;
    }

    @Override
    public LoxObject get(Token name) {
        return null;
    }

    @Override
    public void set(Token name, LoxObject value) {

    }

    @Override
    public boolean has(Token name) {
        return false;
    }
}