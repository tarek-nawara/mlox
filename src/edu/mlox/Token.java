package edu.mlox;

public record Token(TokenType type, String lexeme, Object literal, int line) {
    public static Token fromLexeme(String lexeme) {
        return new Token(null, lexeme, null, 0);
    }
}
